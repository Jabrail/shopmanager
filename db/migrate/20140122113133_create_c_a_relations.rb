class CreateCARelations < ActiveRecord::Migration
  def change
    create_table :c_a_relations do |t|
      t.integer :u_id
      t.integer :c_id
      t.integer :a_id

      t.timestamps
    end
  end
end
