class CreateCardMarkets < ActiveRecord::Migration
  def change
    create_table :card_markets do |t|
      t.string :country
      t.string :city
      t.string :address
      t.has_attached_file :avatar
      t.string :contact_name
      t.integer :m_id

      t.timestamps
    end
  end
  def self.down
    drop_attached_file :card_markets, :avatar
  end
end
