class CreateTagsRelations < ActiveRecord::Migration
  def change
    create_table :tags_relations do |t|
      t.integer :tp_id
      t.integer :tag_id

      t.timestamps
    end
  end
end
