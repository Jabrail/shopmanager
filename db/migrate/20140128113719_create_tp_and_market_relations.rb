class CreateTpAndMarketRelations < ActiveRecord::Migration
  def change
    create_table :tp_and_market_relations do |t|
      t.integer :m_id
      t.integer :tp_id

      t.timestamps
    end
  end
end
