class AddColumnTdIdToCARelations < ActiveRecord::Migration
  def change
    add_column :c_a_relations, :td_id, :integer
  end
end
