class AddColumnAutoIdToWaybillsNames < ActiveRecord::Migration
  def change
    add_column :waybills_names, :auto_id, :integer
  end
end
