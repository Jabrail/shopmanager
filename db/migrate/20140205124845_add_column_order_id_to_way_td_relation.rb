class AddColumnOrderIdToWayTdRelation < ActiveRecord::Migration
  def change
    add_column :way_td_relations, :order_id, :integer
  end
end
