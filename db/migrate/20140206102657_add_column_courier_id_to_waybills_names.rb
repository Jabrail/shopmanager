class AddColumnCourierIdToWaybillsNames < ActiveRecord::Migration
  def change
    add_column :waybills_names, :courier_id, :integer
  end
end
