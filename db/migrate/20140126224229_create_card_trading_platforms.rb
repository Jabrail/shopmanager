class CreateCardTradingPlatforms < ActiveRecord::Migration
  def change
    create_table :card_trading_platforms do |t|
      t.string :country
      t.string :city
      t.string :line
      t.string :rating
      t.has_attached_file :avatar
      t.integer :tp_id

      t.timestamps
    end
  end
  def self.down
    drop_attached_file :card_trading_platforms, :avatar
  end
end
