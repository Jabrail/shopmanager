class CreateAutos < ActiveRecord::Migration
  def change
    create_table :autos do |t|
      t.integer :u_id
      t.string :number
      t.string :type_auto
      t.text :description

      t.timestamps
    end
  end
end
