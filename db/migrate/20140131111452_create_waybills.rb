class CreateWaybills < ActiveRecord::Migration
  def change
    create_table :waybills do |t|
      t.integer :order_id
      t.integer :m_id
      t.integer :courier_id
      t.text :description

      t.timestamps
    end
  end
end
