class CreateCouriers < ActiveRecord::Migration
  def change
    create_table :couriers do |t|
      t.integer :u_id
      t.string :name
      t.string :last_name

      t.timestamps
    end
  end
end
