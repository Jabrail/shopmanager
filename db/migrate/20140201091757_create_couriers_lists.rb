class CreateCouriersLists < ActiveRecord::Migration
  def change
    create_table :couriers_lists do |t|
      t.string :login
      t.string :pass
      t.string :name
      t.string :last_name
      t.integer :td_id

      t.timestamps
    end
  end
end
