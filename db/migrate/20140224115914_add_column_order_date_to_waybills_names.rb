class AddColumnOrderDateToWaybillsNames < ActiveRecord::Migration
  def change
    add_column :waybills_names, :order_date, :string
  end
end
