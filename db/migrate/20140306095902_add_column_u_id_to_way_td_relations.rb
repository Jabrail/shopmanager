class AddColumnUIdToWayTdRelations < ActiveRecord::Migration
  def change
    add_column :way_td_relations, :u_id, :integer
  end
end
