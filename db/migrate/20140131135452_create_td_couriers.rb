class CreateTdCouriers < ActiveRecord::Migration
  def change
    create_table :td_couriers do |t|
      t.string :login
      t.string :pass
      t.string :name
      t.string :last_name
      t.integer :td_id

      t.timestamps
    end
  end
end
