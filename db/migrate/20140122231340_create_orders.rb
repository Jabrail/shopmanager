class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :mp_id
      t.integer :c_a_r_id
      t.integer :item_id
      t.integer :count
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
