class CreateWaybillsNames < ActiveRecord::Migration
  def change
    create_table :waybills_names do |t|
      t.string :name
      t.integer :td_id

      t.timestamps
    end
  end
end
