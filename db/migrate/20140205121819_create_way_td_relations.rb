class CreateWayTdRelations < ActiveRecord::Migration
  def change
    create_table :way_td_relations do |t|
      t.integer :td_id
      t.string :waybill_id
      t.string :integer

      t.timestamps
    end
  end
end
