class AddColumnStatusToWaybill < ActiveRecord::Migration
  def change
    add_column :waybills, :status, :integer
  end
end
