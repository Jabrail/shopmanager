class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.integer :price
      t.string :currency
      t.integer :count
      t.integer :code_number
      t.integer :id_td
      t.integer :category_id
      t.string :description
      t.has_attached_file :image

      t.timestamps
    end

  end

  def self.down
    drop_attached_file :items, :image
  end
end
