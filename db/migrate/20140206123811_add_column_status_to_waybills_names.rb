class AddColumnStatusToWaybillsNames < ActiveRecord::Migration
  def change
    add_column :waybills_names, :status, :integer
  end
end
