class CreateMarketplaces < ActiveRecord::Migration
  def change
    create_table :marketplaces do |t|
      t.integer :u_id
      t.string :name
      t.boolean :status

      t.timestamps
    end
  end
end
