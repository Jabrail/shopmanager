require 'test_helper'

class TdCuriersControllerTest < ActionController::TestCase
  setup do
    @td_curier = td_curiers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:td_couriers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create td_curier" do
    assert_difference('TdCourier.count') do
      post :create, td_curier: { last_name: @td_curier.last_name, login: @td_curier.login, name: @td_curier.name, pass: @td_curier.pass }
    end

    assert_redirected_to td_curier_path(assigns(:td_curier))
  end

  test "should show td_curier" do
    get :show, id: @td_curier
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @td_curier
    assert_response :success
  end

  test "should update td_curier" do
    patch :update, id: @td_curier, td_curier: { last_name: @td_curier.last_name, login: @td_curier.login, name: @td_curier.name, pass: @td_curier.pass }
    assert_redirected_to td_curier_path(assigns(:td_curier))
  end

  test "should destroy td_curier" do
    assert_difference('TdCourier.count', -1) do
      delete :destroy, id: @td_curier
    end

    assert_redirected_to td_curiers_path
  end
end
