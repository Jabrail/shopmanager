require 'test_helper'

class CouriersListsControllerTest < ActionController::TestCase
  setup do
    @couriers_list = couriers_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:couriers_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create couriers_list" do
    assert_difference('CouriersList.count') do
      post :create, couriers_list: { last_name: @couriers_list.last_name, login: @couriers_list.login, name: @couriers_list.name, pass: @couriers_list.pass, td_id: @couriers_list.td_id }
    end

    assert_redirected_to couriers_list_path(assigns(:couriers_list))
  end

  test "should show couriers_list" do
    get :show, id: @couriers_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @couriers_list
    assert_response :success
  end

  test "should update couriers_list" do
    patch :update, id: @couriers_list, couriers_list: { last_name: @couriers_list.last_name, login: @couriers_list.login, name: @couriers_list.name, pass: @couriers_list.pass, td_id: @couriers_list.td_id }
    assert_redirected_to couriers_list_path(assigns(:couriers_list))
  end

  test "should destroy couriers_list" do
    assert_difference('CouriersList.count', -1) do
      delete :destroy, id: @couriers_list
    end

    assert_redirected_to couriers_lists_path
  end
end
