require 'test_helper'

class CardMarketsControllerTest < ActionController::TestCase
  setup do
    @card_market = card_markets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:card_markets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create card_market" do
    assert_difference('CardMarket.count') do
      post :create, card_market: { address: @card_market.address, avatar: @card_market.avatar, city: @card_market.city, contact_name: @card_market.contact_name, country: @card_market.country, m_id: @card_market.m_id }
    end

    assert_redirected_to card_market_path(assigns(:card_market))
  end

  test "should show card_market" do
    get :show, id: @card_market
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @card_market
    assert_response :success
  end

  test "should update card_market" do
    patch :update, id: @card_market, card_market: { address: @card_market.address, avatar: @card_market.avatar, city: @card_market.city, contact_name: @card_market.contact_name, country: @card_market.country, m_id: @card_market.m_id }
    assert_redirected_to card_market_path(assigns(:card_market))
  end

  test "should destroy card_market" do
    assert_difference('CardMarket.count', -1) do
      delete :destroy, id: @card_market
    end

    assert_redirected_to card_markets_path
  end
end
