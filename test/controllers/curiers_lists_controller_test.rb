require 'test_helper'

class CuriersListsControllerTest < ActionController::TestCase
  setup do
    @curiers_list = curiers_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:curiers_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create curiers_list" do
    assert_difference('CuriersList.count') do
      post :create, curiers_list: { last_name: @curiers_list.last_name, login: @curiers_list.login, name: @curiers_list.name, pass: @curiers_list.pass, td_id: @curiers_list.td_id }
    end

    assert_redirected_to curiers_list_path(assigns(:curiers_list))
  end

  test "should show curiers_list" do
    get :show, id: @curiers_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @curiers_list
    assert_response :success
  end

  test "should update curiers_list" do
    patch :update, id: @curiers_list, curiers_list: { last_name: @curiers_list.last_name, login: @curiers_list.login, name: @curiers_list.name, pass: @curiers_list.pass, td_id: @curiers_list.td_id }
    assert_redirected_to curiers_list_path(assigns(:curiers_list))
  end

  test "should destroy curiers_list" do
    assert_difference('CuriersList.count', -1) do
      delete :destroy, id: @curiers_list
    end

    assert_redirected_to curiers_lists_path
  end
end
