require 'test_helper'

class CardTradingPlatformsControllerTest < ActionController::TestCase
  setup do
    @card_trading_platform = card_trading_platforms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:card_trading_platforms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create card_trading_platform" do
    assert_difference('CardTradingPlatform.count') do
      post :create, card_trading_platform: { avatar: @card_trading_platform.avatar, country: @card_trading_platform.country, line: @card_trading_platform.line, rating: @card_trading_platform.rating, sity: @card_trading_platform.sity, tp_id: @card_trading_platform.tp_id }
    end

    assert_redirected_to card_trading_platform_path(assigns(:card_trading_platform))
  end

  test "should show card_trading_platform" do
    get :show, id: @card_trading_platform
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @card_trading_platform
    assert_response :success
  end

  test "should update card_trading_platform" do
    patch :update, id: @card_trading_platform, card_trading_platform: { avatar: @card_trading_platform.avatar, country: @card_trading_platform.country, line: @card_trading_platform.line, rating: @card_trading_platform.rating, sity: @card_trading_platform.sity, tp_id: @card_trading_platform.tp_id }
    assert_redirected_to card_trading_platform_path(assigns(:card_trading_platform))
  end

  test "should destroy card_trading_platform" do
    assert_difference('CardTradingPlatform.count', -1) do
      delete :destroy, id: @card_trading_platform
    end

    assert_redirected_to card_trading_platforms_path
  end
end
