require 'test_helper'

class WaybillsControllerTest < ActionController::TestCase
  setup do
    @waybill = waybills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:waybills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create waybill" do
    assert_difference('Waybill.count') do
      post :create, waybill: { : @waybill., courier_id: @waybill.courier_id, description: @waybill.description, m_id: @waybill.m_id, order_id: @waybill.order_id }
    end

    assert_redirected_to waybill_path(assigns(:waybill))
  end

  test "should show waybill" do
    get :show, id: @waybill
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @waybill
    assert_response :success
  end

  test "should update waybill" do
    patch :update, id: @waybill, waybill: { : @waybill., courier_id: @waybill.courier_id, description: @waybill.description, m_id: @waybill.m_id, order_id: @waybill.order_id }
    assert_redirected_to waybill_path(assigns(:waybill))
  end

  test "should destroy waybill" do
    assert_difference('Waybill.count', -1) do
      delete :destroy, id: @waybill
    end

    assert_redirected_to waybills_path
  end
end
