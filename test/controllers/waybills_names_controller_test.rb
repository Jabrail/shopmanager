require 'test_helper'

class WaybillsNamesControllerTest < ActionController::TestCase
  setup do
    @waybills_name = waybills_names(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:waybills_names)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create waybills_name" do
    assert_difference('WaybillsName.count') do
      post :create, waybills_name: { name: @waybills_name.name, td_id: @waybills_name.td_id }
    end

    assert_redirected_to waybills_name_path(assigns(:waybills_name))
  end

  test "should show waybills_name" do
    get :show, id: @waybills_name
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @waybills_name
    assert_response :success
  end

  test "should update waybills_name" do
    patch :update, id: @waybills_name, waybills_name: { name: @waybills_name.name, td_id: @waybills_name.td_id }
    assert_redirected_to waybills_name_path(assigns(:waybills_name))
  end

  test "should destroy waybills_name" do
    assert_difference('WaybillsName.count', -1) do
      delete :destroy, id: @waybills_name
    end

    assert_redirected_to waybills_names_path
  end
end
