require 'test_helper'

class MarketManageControllerTest < ActionController::TestCase
  test "should get search_tp" do
    get :search_tp
    assert_response :success
  end

  test "should get list_td" do
    get :list_td
    assert_response :success
  end

  test "should get orders" do
    get :orders
    assert_response :success
  end

  test "should get settings" do
    get :settings
    assert_response :success
  end

end
