require 'test_helper'

class PublicTpControllerTest < ActionController::TestCase
  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get trade" do
    get :trade
    assert_response :success
  end

  test "should get review" do
    get :review
    assert_response :success
  end

end
