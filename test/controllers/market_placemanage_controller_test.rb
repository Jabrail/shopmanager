require 'test_helper'

class MarketPlacemanageControllerTest < ActionController::TestCase
  test "should get main" do
    get :main
    assert_response :success
  end

  test "should get orders" do
    get :orders
    assert_response :success
  end

  test "should get followers" do
    get :followers
    assert_response :success
  end

  test "should get orders" do
    get :orders
    assert_response :success
  end

  test "should get category_manage" do
    get :category_manage
    assert_response :success
  end

  test "should get item_manage" do
    get :item_manage
    assert_response :success
  end

  test "should get settings" do
    get :settings
    assert_response :success
  end

end
