Shopmanager::Application.routes.draw do
  devise_for :market_users
  devise_for :courier_profiles
  devise_for :my_couriers
  devise_for :couriers
  resources :couriers_lists

  resources :curiers_lists

  resources :td_couriers

  resources :waybills_names
  post "waybills_names/add_relation"
  post "waybills_names/remove_relation"
  post "waybills_names/add_auto_courier"
  post "waybills_names/set_status"
  post "waybills_names/get_courier_loc"

  resources :waybills

  get "market_placemanage/main"
  get "market_placemanage/followers"
  get "market_placemanage/orders"
  get "market_placemanage/category_manage"
  get "market_placemanage/item_manage"
  get "market_placemanage/settings"
  get "market_placemanage/courier_manage"
  resources :tags

  get "public_tp/show"
  get "public_tp/trade"
  get "public_tp/review"
  get "market_manage/search_tp"
  get "market_manage/list_td"
  get "market_manage/orders"
  get "market_manage/settings"
  get "market_manage/add_tp"

  resources :card_markets

  resources :card_trading_platforms

  resources :markets

  resources :categories

  resources :items

  resources :types

  resources :orders
  post "orders/add_order"

  resources :couriers

  resources :autos

  get "default_pages/login"
  get "default_pages/market"
  get "market" => "default_pages#market"
  get "trader" => "default_pages#login"
  devise_for :users
  get "persons/profile"
  get "persons/market_profile"
  get "persons/market"
  get "persons/courier"
  get "persons/marketPlace"
  resources :marketplaces

  resources :users

  # The priority is based upon order btn btn-primaryof creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'default_pages#login'

  get 'persons/profile', as: 'user_root'

  get 'persons/market_profile', as: 'market_user_root'


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
