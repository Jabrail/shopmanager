class CouriersListsController < ApplicationController
  before_action :set_couriers_list, only: [:show, :edit, :update, :destroy]

  # GET /couriers_lists
  # GET /couriers_lists.json
  def index
    @couriers_lists = CouriersList.where(td_id: params[:td_id], u_id: current_user.id)
  end

  # GET /couriers_lists/1
  # GET /couriers_lists/1.json
  def show
    @couriers_list = CouriersList.where(id: params[:id] , U_id: current_user.id).take
  end

  # GET /couriers_lists/new
  def new
    @couriers_list = CouriersList.new
  end

  # GET /couriers_lists/1/edit
  def edit
  end

  # POST /couriers_lists
  # POST /couriers_lists.json
  def create
    @couriers_list = CouriersList.new(couriers_list_params)
    @couriers_list.td_id = params[:td_id]
    @couriers_list.u_id = current_user.id

    respond_to do |format|
      if @couriers_list.save
        format.html { redirect_to @couriers_list, notice: 'Couriers list was successfully created.' }
        format.json { render action: 'show', status: :created, location: @couriers_list }
      else
        format.html { render action: 'new' }
        format.json { render json: @couriers_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /couriers_lists/1
  # PATCH/PUT /couriers_lists/1.json
  def update
    respond_to do |format|
      if @couriers_list.update(couriers_list_params)
        format.html { redirect_to @couriers_list, notice: 'Couriers list was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @couriers_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /couriers_lists/1
  # DELETE /couriers_lists/1.json
  def destroy
    @couriers_list.destroy
    respond_to do |format|
      format.html { redirect_to couriers_lists_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_couriers_list
      @couriers_list = CouriersList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def couriers_list_params
      params.require(:couriers_list).permit(:login, :pass, :name, :last_name, :td_id)
    end
end
