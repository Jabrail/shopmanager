class TdCouriersController < ApplicationController
  before_action :set_td_curier, only: [:show, :edit, :update, :destroy]

  # GET /td_couriers
  # GET /td_couriers.json
  def index
    @td_couriers = TdCourier.find_all_by_td_id(user_session[:current_trading_place])
  end

  # GET /td_couriers/1
  # GET /td_couriers/1.json
  def show
  end

  # GET /td_couriers/new
  def new
    @td_courier = TdCourier.new
  end

  # GET /td_couriers/1/edit
  def edit
  end

  # POST /td_couriers
  # POST /td_couriers.json
  def create
    @td_courier = TdCourier.new()
    @td_courier.login = params[:login]
    @td_courier.pass = params[:pass]
    @td_courier.name = params[:name]
    @td_courier.last_name = params[:last_name]
    @td_courier.td_id = user_session[:current_trading_place]

    respond_to do |format|
      if @td_courier.save
        format.html { redirect_to @td_courier, notice: 'Td curier was successfully created.' }
        format.json { render action: 'show', status: :created, location: @td_courier }
      else
        format.html { render action: 'new' }
        format.json { render json: @td_courier.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /td_couriers/1
  # PATCH/PUT /td_couriers/1.json
  def update
    respond_to do |format|
      if @td_courier.update(td_courier_params)
        format.html { redirect_to @td_courier, notice: 'Td curier was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @td_courier.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /td_couriers/1
  # DELETE /td_couriers/1.json
  def destroy
    @td_courier.destroy
    respond_to do |format|
      format.html { redirect_to td_couriers_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_td_courier
      @td_courier = TdCourier.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def td_cuorier_params
      params.require(:td_courier).permit(:login, :pass, :name, :last_name)
    end
end
