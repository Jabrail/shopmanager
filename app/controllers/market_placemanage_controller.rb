class MarketPlacemanageController < ApplicationController
  def main
    user_session[:current_trading_place] = params[:td_id]
  end



  def followers
  end

  def orders
    @m_id = params[:td_id]
    @orders = Order.where(mp_id: params[:td_id] , u_id: current_user.id)
  end

  def category_manage
    if params[:id]
    @categories = Category.where(m_id: user_session[:current_trading_place] , parentCat: params[:id] )
    @items = Item.where(id_td: user_session[:current_trading_place] , category_id: params[:id] )
    else
    @categories = Category.where(m_id: user_session[:current_trading_place] , parentCat: 0 )
    @items = Item.where(id_td: user_session[:current_trading_place] , category_id: 0 )
    end

  end

  def item_manage
  end

  def settings
  end

  def courier_manage

  end

  def add_auto_car_relation

  end
end
