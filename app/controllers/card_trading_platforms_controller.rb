class CardTradingPlatformsController < ApplicationController
  before_action :set_card_trading_platform, only: [:show, :edit, :update, :destroy]

  # GET /card_trading_platforms
  # GET /card_trading_platforms.json
  def index
    if CardTradingPlatform.find_by_tp_id(user_session[:current_trading_place])
      @card_trading_platform = CardTradingPlatform.find_by_tp_id(user_session[:current_trading_place])
    else
      redirect_to new_card_trading_platform_url
     end
    end

  # GET /card_trading_platforms/1
  # GET /card_trading_platforms/1.json
  def show
  end

  # GET /card_trading_platforms/new
  def new
    if CardTradingPlatform.find_by_tp_id(user_session[:current_trading_place])
      redirect_to card_trading_platforms_url
    else
      @card_trading_platform = CardTradingPlatform.new
    end
  end

  # GET /card_trading_platforms/1/edit
  def edit
  end

  # POST /card_trading_platforms
  # POST /card_trading_platforms.json
  def create
    @card_trading_platform = CardTradingPlatform.new(card_trading_platform_params)
    @card_trading_platform.tp_id = user_session[:current_trading_place]

    respond_to do |format|
      if @card_trading_platform.save
        format.html { redirect_to @card_trading_platform, notice: 'Card trading platform was successfully created.' }
        format.json { render action: 'show', status: :created, location: @card_trading_platform }
      else
        format.html { render action: 'new' }
        format.json { render json: @card_trading_platform.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /card_trading_platforms/1
  # PATCH/PUT /card_trading_platforms/1.json
  def update
    respond_to do |format|
      if @card_trading_platform.update(card_trading_platform_params)
        format.html { redirect_to @card_trading_platform, notice: 'Card trading platform was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @card_trading_platform.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /card_trading_platforms/1
  # DELETE /card_trading_platforms/1.json
  def destroy
    @card_trading_platform.destroy
    respond_to do |format|
      format.html { redirect_to card_trading_platforms_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_card_trading_platform
      @card_trading_platform = CardTradingPlatform.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def card_trading_platform_params
      params.require(:card_trading_platform).permit(:country, :sity, :line, :rating, :avatar, :tp_id)
    end
end
