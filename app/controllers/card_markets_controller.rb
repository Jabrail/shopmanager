class CardMarketsController < ApplicationController
  before_action :set_card_market, only: [:show, :edit, :update, :destroy]

  # GET /card_markets
  # GET /card_markets.json
  def index
    #if CardMarket.find_by_m_id(user_session[:current_market])
      @card_markets = CardMarket.all
      #@card_markets = CardMarket.find_by_m_id(user_session[:current_market])
    #else
      #redirect_to card_markets_path
    #end
  end


  # GET /card_markets/1
  # GET /card_markets/1.json
  def show
    @card_market = CardMarket.find_by_m_id(user_session[:current_market])
  end

  # GET /card_markets/new
  def new
    if CardMarket.find_by_m_id(user_session[:current_market])
      redirect_to card_markets_url
    else
      @card_market = CardMarket.new
    end
  end

  # GET /card_markets/1/edit
  def edit
  end

  # POST /card_markets
  # POST /card_markets.json
  def create
    @card_market = CardMarket.new(card_market_params)
    @card_market.m_id = user_session[:current_market]

    respond_to do |format|
      if @card_market.save
        format.html { redirect_to @card_market, notice: 'Card market was successfully created.' }
        format.json { render action: 'show', status: :created, location: @card_market }
      else
        format.html { render action: 'new' }
        format.json { render json: @card_market.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /card_markets/1
  # PATCH/PUT /card_markets/1.json
  def update
    respond_to do |format|
      if @card_market.update(card_market_params)
        format.html { redirect_to @card_market, notice: 'Card market was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @card_market.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /card_markets/1
  # DELETE /card_markets/1.json
  def destroy
    @card_market.destroy
    respond_to do |format|
      format.html { redirect_to card_markets_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_card_market
      @card_market = CardMarket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def card_market_params
      params.require(:card_market).permit(:country, :city, :address, :avatar, :contact_name, :m_id)
    end
end
