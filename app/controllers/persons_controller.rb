class PersonsController < ApplicationController
  def profile

      @marketPlaces = Marketplace.find_all_by_u_id(current_user.id)

  end

  def courier
    user_session[:current_courier] = params[:id]
  end

  def market

  end

  def marketPlace
    user_session[:current_trading_place] = params[:id]
  end

  def market_profile
    @markets = Market.find_all_by_u_id(current_market_user.id)
  end
end
