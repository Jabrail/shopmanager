class DefaultPagesController < ApplicationController
  def login
    if current_user
      redirect_to persons_profile_url
    else
      render :layout => false
    end

  end

  def market

    render :layout => false
  end
end
