class WaybillsNamesController < ApplicationController
  before_action :set_waybills_name, only: [:show, :edit, :update, :destroy]
  #skip_before_filter :verify_authenticity_token

  # GET /waybills_names
  # GET /waybills_names.json
  def index
    @waybills_names = WaybillsName.where(td_id: params[:td_id] , u_id:current_user.id)
  end

  # GET /waybills_names/1
  # GET /waybills_names/1.json
  def show

    @relation = WayTdRelations.where(td_id: params[:td_id] , waybill_id: params[:id] , u_id: current_user.id)
    @waybill_id =  params[:id]

  end

  # GET /waybills_names/new
  def new
    @waybills_name = WaybillsName.new
  end

  # GET /waybills_names/1/edit
  def edit
  end

  # POST /waybills_names
  # POST /waybills_names.json
  def create
    @waybills_name = WaybillsName.new(waybills_name_params)
    @waybills_name.td_id = params[:td_id]
    @waybills_name.u_id = current_user.id
    @waybills_name.status = 0

    @waybills_name.save
    redirect_to market_placemanage_orders_url+'?td_id='+params[:td_id]
  end

  # PATCH/PUT /waybills_names/1
  # PATCH/PUT /waybills_names/1.json
  def update
    respond_to do |format|
      if @waybills_name.update(waybills_name_params)
        format.html { redirect_to @waybills_name, notice: 'Waybills name was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @waybills_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /waybills_names/1
  # DELETE /waybills_names/1.json
  def destroy
    @waybills_name.destroy
    respond_to do |format|
      format.html { redirect_to waybills_names_url }
      format.json { head :no_content }
    end
  end

  def add_relation

    if (!WayTdRelations.find_by_order_id(params[:order_id]))
      @relation = WayTdRelations.new()
      @relation.order_id  = params[:order_id]
      @relation.td_id  = params[:td_id]
      @relation.waybill_id  = params[:wayBill]
      @relation.u_id  = current_user.id
      @relation.save

      Order.update(params[:order_id], :status => 3)

    end
    redirect_to market_placemanage_orders_url
  end
  def add_auto_courier

    if WaybillsName.find(params[:waybill_id]).u_id == current_user.id

      if params[:waybill_id]

        waybill = WaybillsName.find(params[:waybill_id])

        if params[:auto]

          waybill.update(:auto_id => params[:auto])

        end

        if params[:courier]

          waybill.update(:courier_id => params[:courier])

        end

        if params[:order_date]

          waybill.update(:order_date => params[:order_date])

        end
      end
    end

    render :layout => nil
  end

  def set_status

    if WaybillsName.find(params[:waybill_id]).u_id == current_user.id
      WaybillsName.update(params[:waybill_id] , :status => params[:status])
    end
    redirect_to market_placemanage_orders_url
  end

  def remove_relation


    @relation = WayTdRelations.find(params[:id])
    if @relation.u_id == current_user.id
      Order.update(@relation.order_id, :status => 0)

      @relation.destroy
    end
    render :layout => nil
  end

  def get_courier_loc

    @loc = CourierLocation.find_last_by_c_id(params[:id])

    render  json: { loc: @loc }

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_waybills_name
    @waybills_name = WaybillsName.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def waybills_name_params
    params.require(:waybills_name).permit(:name, :td_id)
  end
end
