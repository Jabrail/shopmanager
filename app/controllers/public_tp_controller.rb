class PublicTpController < ApplicationController
  def show
    @marketPlace = Marketplace.find(params[:id])
  end

  def trade
    if params[:id]
      @categories = Category.where(td_id: params[:td_id], parentCat: params[:id] )
      @items = Item.where(id_td: params[:td_id] , category_id: params[:id] )
      @m_id = params[:td_id]
    else
      @categories = Category.where(td_id: params[:td_id] , parentCat: 0 )
      @items = Item.where(id_td: params[:td_id] , category_id: 0 )
      @m_id = params[:td_id]
    end
  end

  def review
  end
end
