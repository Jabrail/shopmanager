class MarketManageController < ApplicationController
  def search_tp
    if params[:name]
      @marketPlaces = Marketplace.find_all_by_name(params[:name])
    end
  end

  def list_td
    @relations = TpAndMarketRelation.find_all_by_m_id(params[:m_id])
  end

  def orders
    @orders = Order.find_all_by_c_a_r_id(params[:m_id])
  end

  def settings
  end

  def add_tp
    relation = TpAndMarketRelation.new()
    relation.m_id = params[:m_id]
    relation.tp_id = params[:td_id]
    relation.save
    render :layout => nil

  end
end
