class MarketplacesController < ApplicationController
  before_action :set_marketplace, only: [:show, :edit, :update, :destroy]

  # GET /marketplaces
  # GET /marketplaces.json
  def index
    @marketplaces = Marketplace.find_all_by_u_id(current_user.id)
  end

  # GET /marketplaces/1
  # GET /marketplaces/1.json
  def show
  end

  # GET /marketplaces/new
  def new
    @marketplace = Marketplace.new
  end

  # GET /marketplaces/1/edit
  def edit
  end

  # POST /marketplaces
  # POST /marketplaces.json
  def create
    @marketplace = Marketplace.new(marketplace_params)
    @marketplace.u_id = current_user.id
    @marketplace.status = 'true'
    respond_to do |format|
      if @marketplace.save
        format.html { redirect_to @marketplace, notice: 'Marketplace was successfully created.' }
        format.json { render action: 'show', status: :created, location: @marketplace }
      else
        format.html { render action: 'new' }
        format.json { render json: @marketplace.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /marketplaces/1
  # PATCH/PUT /marketplaces/1.json
  def update
    respond_to do |format|
      if @marketplace.update(marketplace_params)
        format.html { redirect_to @marketplace, notice: 'Marketplace was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @marketplace.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /marketplaces/1
  # DELETE /marketplaces/1.json
  def destroy
    @marketplace.destroy
    respond_to do |format|
      format.html { redirect_to marketplaces_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_marketplace
      @marketplace = Marketplace.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def marketplace_params
      params.require(:marketplace).permit(:u_id, :name, :status)
    end
end
