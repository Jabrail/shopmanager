json.array!(@couriers) do |courier|
  json.extract! courier, :id, :u_id, :name, :last_name
  json.url courier_url(courier, format: :json)
end
