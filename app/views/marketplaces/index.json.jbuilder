json.array!(@marketplaces) do |marketplace|
  json.extract! marketplace, :id, :u_id, :name, :status
  json.url marketplace_url(marketplace, format: :json)
end
