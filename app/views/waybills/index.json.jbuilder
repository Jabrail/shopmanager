json.array!(@waybills) do |waybill|
  json.extract! waybill, :id, :order_id, :m_id, :courier_id, :, :description
  json.url waybill_url(waybill, format: :json)
end
