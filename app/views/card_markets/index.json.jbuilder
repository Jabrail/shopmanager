json.array!(@card_markets) do |card_market|
  json.extract! card_market, :id, :country, :city, :address, :avatar, :contact_name, :m_id
  json.url card_market_url(card_market, format: :json)
end
