json.array!(@card_trading_platforms) do |card_trading_platform|
  json.extract! card_trading_platform, :id, :country, :sity, :line, :rating, :avatar, :tp_id
  json.url card_trading_platform_url(card_trading_platform, format: :json)
end
