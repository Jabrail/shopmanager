json.array!(@couriers_lists) do |couriers_list|
  json.extract! couriers_list, :id, :login, :pass, :name, :last_name, :td_id
  json.url couriers_list_url(couriers_list, format: :json)
end
